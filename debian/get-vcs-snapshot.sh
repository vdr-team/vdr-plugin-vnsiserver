echo Downloading latest from git repository...
set -e

DEB_SOURCE_PACKAGE=`egrep '^Source: ' debian/control | cut -f 2 -d ' '`
VERSION_UPSTREAM=`dpkg-parsechangelog | grep ^Version: | sed -e 's/^Version:\s*//' -e s/-[^-]*// | sed -e 's/\+git.*//'`
VERSION_DATE=`/bin/date --utc +%0Y%0m%0d`
VERSION_FULL="${VERSION_UPSTREAM}+git${VERSION_DATE}"

git clone --depth 1 https://github.com/FernetMenta/vdr-plugin-vnsiserver

GIT_SHA=`git --work-tree xbmc-pvr-addons show --pretty=format:"%h" --quiet | head -1 || true`

make -C "vdr-plugin-vnsiserver" dist

mv vdr-plugin-vnsiserver/*.tgz "../${DEB_SOURCE_PACKAGE}_${VERSION_FULL}.orig.tar.gz"

rm -rf vdr-plugin-vnsiserver

git-import-orig --pristine-tar "../${DEB_SOURCE_PACKAGE}_${VERSION_FULL}.orig.tar.gz"
dch -v "$VERSION_FULL-1" "New Upstream Snapshot (commit $GIT_SHA)"
